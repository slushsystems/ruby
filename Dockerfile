FROM slushsystems/fedora-base:latest

USER root

RUN dnf -y install git ruby ruby-devel redhat-rpm-config \
  && dnf -y groupinstall "Development Tools" \
  && gem install bundler \
  && dnf clean all

USER slushy

